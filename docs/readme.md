# Документация на мой API

Перед Вами самый крутой в мире Список Покупок!

shopping_list : `{product(str) : amount(int)}`

POST: 

  - request data: `{'product' : {product_name}, 'amount' : {amount_of_product}}`
    
  - добавить новую запись в список
    
  - если продукт уже в списке, сообщить об этом

PUT:

  - request data: `{'product' : {product_name}, 'amount' : {amount_of_product}}`
   
  - изменить количество продукта
    
  - если продукта не было в списке, сообщить об этом

GET:

  - получить полный список продуктов

GET/product:

  - request data: `{'product' : {product_name}}`
    
  - получить данные о продукте

DELETE:

  - request data: `{'product' : {product_name}}`
    
  - удалить продукт из списка