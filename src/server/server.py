import os

from flask import Flask
from flask import request
app = Flask(__name__)

shopping_list = {}
 
@app.route('/', methods=['POST'])
def post_product():
    form = request.form
    product = form.get('product')
    amount = form.get('amount')
    if product in shopping_list.keys():
        return 'Product already exists', 400
    shopping_list[product] = amount
    return 'OK', 200
 
@app.route('/', methods=['PUT'])
def put_product():
    form = request.form
    product = form.get('product')
    amount = form.get('amount')
    if product not in shopping_list.keys():
        return 'Product does not exist', 400
    else:
        shopping_list[product] = amount
        return 'OK', 200
 
@app.route('/', methods=['GET'])
def get_list(product=None):   
    return shopping_list, 200
 
@app.route('/product', methods=['GET'])
def get_product():   
    form = request.form
    product = form.get('product')
    if product not in shopping_list.keys():
        return 'Product does not exist', 400
    else:
        return f'{product} : {shopping_list[product]}\n', 200
 
@app.route('/', methods=['DELETE'])
def delete_product(): 
    form = request.form
    product = form.get('product')
    if product in shopping_list.keys():
        shopping_list.pop(product)
    return 'OK', 200

# @app.route('/')
# def hello_world():
#     return 'Hello, World!'


app.run(host='0.0.0.0', port=int(os.environ.get('HSE_HTTP_FLASK_PORT', 80)))
