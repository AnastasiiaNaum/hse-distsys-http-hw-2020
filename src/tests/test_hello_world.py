import os

import requests

SERVER_HOST = os.environ.get('HSE_HTTP_TESTS_SERVER_HOST', 'server')
SERVER_PORT = int(os.environ.get('HSE_HTTP_FLASK_PORT', 80))
URL = 'http://' + SERVER_HOST
if SERVER_PORT != 80:
    URL += ':{}'.format(SERVER_PORT)

product1 = {'product' : 'Milk', 'amount' : 1}
product2 = {'product' : 'Banana', 'amount' : 5}
product1_upd = {'product' : 'Milk', 'amount' : 2}
product1_name = {'product' : 'Milk'}
product2_name = {'product' : 'Banana'}
 
def test_empty():
    response = requests.get(URL + '/')
    assert response.text == '{}\n'
    assert response.status_code == 200
 
def test_NewList():
    response = requests.post(URL + '/', data=product1)
    assert response.text == 'OK'
    assert response.status_code == 200
 
def test_ExistsList():
    response = requests.post(URL + '/', data=product1)
    assert response.text == 'Product already exists'
    assert response.status_code == 400
 
def test_UpdList():
    response = requests.put(URL + '/', data=product1_upd)
    assert response.text == 'OK'
    assert response.status_code == 200
 
def test_WrongUpdList():
    response = requests.put(URL + '/', data=product2)
    assert response.text == 'Product does not exist'
    assert response.status_code == 400
 
def test_GetList():
    response = requests.get(URL + '/')
    assert response.text == '{"Milk":"2"}\n'
    assert response.status_code == 200
 
def test_GetProduct():
    response = requests.get(URL + '/product', data=product1_name)
    assert response.text == 'Milk : 2\n'
    assert response.status_code == 200
 
def test_WrongGetProduct():
    response = requests.get(URL + '/product', data=product2_name)
    assert response.text == 'Product does not exist'
    assert response.status_code == 400
 
def test_DeleteProduct():
    response = requests.delete(URL + '/', data=product1_name)
    assert response.text == 'OK'
    assert response.status_code == 200


# def test_hello_world():
#     response = requests.get(URL + '/')
#     assert response.text == 'Hello, World!'
#     assert response.status_code == 200
